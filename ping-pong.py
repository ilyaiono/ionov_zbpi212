import tkinter as tk
import time


def move_bat():
    global batX
    inRangeLeft = batX > 49
    inRangeRight = batX < 451
    if rightPressed and inRangeRight:
        batX = batX + 10
    if leftPressed and inRangeLeft:
        batX = batX - 10


def move_ball(speed):
    global ballX, ballY, ballMoveX, ballMoveY, score
    ballX = ballX + ballMoveX
    ballY = ballY + ballMoveY
    touchingBat = ballX > batX-51 and ballX < batX+51 and ballY > 439
    if ballX > 489:
        ballMoveX = -5
    elif ballX < 11:
        ballMoveX = 5
    else:
        pass
    if ballY > 489:
        ballMoveY = -5
    elif ballY < 11:
        ballMoveY = 5
    else:
        pass
    if touchingBat:
        ballMoveY = ballMoveY - ballMoveY * 2
        score += 1
        if speed != 1:
            speed -= 1
    return speed


def locate_objects():
    game.coords(bat, batX-50, 460, batX+50, 440)
    game.coords(ball, ballX-10, ballY-10, ballX+10, ballY+10)
    root.update()


def press(event):
    global leftPressed, rightPressed
    if event.keysym == 'a':
        leftPressed = 1
    if event.keysym == 'd':
        rightPressed = 1


def release(event):
    global leftPressed, rightPressed
    if event.keysym == 'a':
        leftPressed = 0
    if event.keysym == 'd':
        rightPressed = 0


root = tk.Tk()
root.title("Пинг-Понг")
HEIGHT = WIDTH = 500
game = tk.Canvas(root, width=WIDTH, height=HEIGHT, bg='#000000')
game.pack()

bat = game.create_rectangle(0, 0, 100, 20, fill='#F2BA49')
ball = game.create_oval(0, 0, 20, 20, fill='#FF0000')

batX = 250
ballX = 250
ballY = 250
ballMoveY = 5
ballMoveX = 5
rightPressed = 0
leftPressed = 0
gameOver = False
score = 0
speed = 18

T = tk.Label(root, text=f'Очки: {score}')
T.pack()

root.bind('<KeyPress>', press)
root.bind('<KeyRelease>', release)

locate_objects()

# задержка перед началом игры
time.sleep(2)
while not gameOver:
    move_bat()
    speed = move_ball(speed)
    locate_objects()
    T.config(text=f'Очки: {score}')
    if ballY > 470:
        gameOver = True
    # скорость
    root.after(speed)

print(f'Игра закончена!\nУ вас {score} очков!')

