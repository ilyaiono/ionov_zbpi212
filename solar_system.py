import tkinter as tk
import math


def calculate_position(data):
    center_x, center_y, radius, distance, angle, angle_speed, x, y = data

    # вычисляю местоположение объекта
    x = center_x - distance * math.sin(math.radians(-angle))
    y = center_y - distance * math.cos(math.radians(-angle))

    # сохраняю позицию для использования в качестве центра
    data[6] = x
    data[7] = y

    # вычисляю координаты круга
    x1 = x - radius
    y1 = y - radius
    x2 = x + radius
    y2 = y + radius

    return x1, y1, x2, y2


def create_object(data, fill_color, line_color):
    x1, y1, x2, y2 = calculate_position(data)
    return c.create_oval(x1, y1, x2, y2, fill=fill_color, outline=line_color)


def move_object(object_id, data):
    x1, y1, x2, y2 = calculate_position(data)
    c.coords(object_id, x1, y1, x2, y2)


def animate():
    # для перемещения земли прибавляем к current_angle angle_speed
    earth[4] += earth[5]
    move_object(e_id, earth)

    # использую позицию земли для центра вращения луны
    moon[0] = earth[6]
    moon[1] = earth[7]

    # для перемещения луны прибавляем к current_angle angle_speed
    moon[4] += moon[5]
    move_object(m_id, moon)

    # скорость анимации
    root.after(50, animate)


# размер окна
HEIGHT = WIDTH = 800

# центр системы
center_x = WIDTH//2
center_y = HEIGHT//2

# данные небесных тел
# [center of rotation x/y, radius, distance from center, current angle, angle speed, current positon x/y]
sun = [center_x, center_y, 100, 0, 0, 0, 0, 0]
earth = [center_x, center_y, 10, 300, 0, 1, 0, 0]
moon = [0, 0, 5, 40, 0, 3, 0, 0]

root = tk.Tk()
root.title("Солнечная система")

c = tk.Canvas(root, width=WIDTH, height=HEIGHT, bg='#1F282D')
c.pack()

# создаю солнце и луну
s_id = create_object(sun, '#F2BA49', '#AA4A44')
e_id = create_object(earth, '#088F8F', '#1F282D')

# использование земли в качестве центра для луны
moon[0] = earth[6]
moon[1] = earth[7]

# создаю луну
m_id = create_object(moon, '#808080', '#000000')

animate()

root.mainloop()