import tkinter as tk
import colorsys


def hilbert(index):
    """возвращаю координаты точек"""
    points = [[0, 0], [0, 1], [1, 1], [1, 0]]

    newI = index & 3

    v = points[newI]

    for i in range(order):
        i += 1
        index = index >> 2
        newI = index & 3
        le = 2 ** i
        if newI == 0:
            z = v[0]
            v[0] = v[1]
            v[1] = z
        elif newI == 1:
            v[1] += le
        elif newI == 2:
            v[0] += le
            v[1] += le
        elif newI == 3:
            z = le - 1 - v[0]
            v[0] = le - 1 - v[1]
            v[1] = z
            v[0] += le
    return v


def createPoints():
    """создаю список всех вершин"""
    global path
    path = []
    for point in range(total):
        path.append(hilbert(point))
        l = WIDTH / N
        path[point] = [x * l for x in path[point]]
        path[point] = [x + (l / 2) for x in path[point]]


def draw():
    global counter
    global order
    global N
    global total
    i = counter
    speed = 1

    if order < 4:
        speed = 20

    # генерирую цвет
    rgb = tuple([int(round(x * 255, 0)) for x in colorsys.hls_to_rgb(i / total, 0.5, 1)])
    color = '#%02x%02x%02x' % rgb

    c.create_line(path[i], path[i + 1], fill=color)
    counter += 1

    if counter >= total - 1:
        counter = 0
        # остановка отрисовки
        if order > 5:
            return
        order += 1
        N = 2 ** order
        total = N ** 2
        createPoints()
        c.delete("all")

    c.after(speed, draw)


path = []
counter = 0
order = 1
N = 2 ** order
total = N ** 2
HIGHT = WIDTH = 800

root = tk.Tk()
root.title('Кривая Гильберта')
c = tk.Canvas(root, width=WIDTH, height=HIGHT, bg='#000000')
c.pack()

createPoints()

draw()
root.mainloop()